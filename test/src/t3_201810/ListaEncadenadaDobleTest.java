package t3_201810;


import junit.framework.TestCase;
import model.data_structures.ListaEncadenadaDoble;
import model.data_structures.Node;

public class ListaEncadenadaDobleTest<T> extends TestCase {
	
	private ListaEncadenadaDoble listaPrueba;
	private Node nodoPrueba;
	
	
	public void setUp1()
	{
		listaPrueba= new ListaEncadenadaDoble();
			for(int i=0;i<10;i++)
			{
				listaPrueba.addLast(i);
			}
	}
	
	public void testSize()
	{
		setUp1();
		assertEquals(10,listaPrueba.size());
	}
	
	public void testAddLast()
	{
		setUp1();
		int num = 100;
		listaPrueba.addLast(num);
		assertEquals(100,listaPrueba.get(10));
	}
	
	public void testAddFirst()
	{
		setUp1();
		listaPrueba.addFirst(100);
		assertEquals(100,listaPrueba.get(0));
	}
	
	public void testDelete()
	{
		setUp1();
		listaPrueba.delete(5);
		assertEquals(9,listaPrueba.size());
		assertEquals(6,listaPrueba.get(5));
	}
	
	public void testNext()
	{
		setUp1();
		listaPrueba.next();
		assertEquals(1,listaPrueba.current.getItem());
	}
	
	public void testListing()
	{
		setUp1();
		listaPrueba.listing();
		assertEquals(0,listaPrueba.current.getItem());
		
	}
	
	public void testGetCurrent()
	{
		setUp1();
		assertEquals(0,listaPrueba.getCurrent());
	}
	
	public void testGetElement()
	{
		setUp1();
		assertEquals(5,listaPrueba.get(5));
	}
	
	public void testGetPosition()
	{
		setUp1();
		assertEquals(4,listaPrueba.get(4));
	}
	
	public void testDeleteFirst()
	{
		setUp1();
		listaPrueba.deleteFirst();
		assertEquals(1, listaPrueba.getFirst());
	}
	
	public void testDeleteLast()
	{
		setUp1();
		listaPrueba.deleteLast();
		assertEquals(8,listaPrueba.getLast());
	}
	
	public void getLast()
	{
		setUp1();
		assertEquals(0, listaPrueba.getFirst());
	}
	
	public void getFirst()
	{
		setUp1();
		assertEquals(9, listaPrueba.getLast());
	}
}


package t3_201810;

import junit.framework.TestCase;
import model.data_structures.Queue;

public class QueueTest<T> extends TestCase {
	
	private Queue prueba;
	
	public void setUpEscenario()
	{
		prueba = new Queue();
		
		for( int i =0; i<10;i++)
		{
			prueba.enqueue(i);
		}
	}
	
	public void setUpEscenario1()
	{
		prueba = new Queue();
	}
	
	public void enqueueTest()
	{
		setUpEscenario1();
		
		prueba.enqueue(100);
		assertEquals(100, prueba.dequeue());
	}
	
	public void dequeueTest()
	{
		setUpEscenario();
		assertEquals(0, prueba.dequeue());
		assertEquals(1, prueba.dequeue());
		assertEquals(2, prueba.dequeue());
	}
	
	public void isEmptyTest()
	{
		setUpEscenario1();
		assertEquals(false,prueba.isEmpty());
		
		setUpEscenario();
		assertEquals(true, prueba.isEmpty());
	}
	
	public void sizeTest()
	{
		setUpEscenario1();
		assertEquals(10,prueba.size());
	}
	
}

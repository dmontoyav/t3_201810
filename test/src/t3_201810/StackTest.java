package t3_201810;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase {
	
	Stack prueba=new Stack();
	
	public void setUp1()
	{
		for(int i =0; i<10;i++)
		{
			prueba.push(i);
		}
	}
	
	public void testPop()
	{
		assertEquals(null,prueba.pop());
		
		setUp1();
		assertEquals(9,prueba.pop());
		assertEquals(8,prueba.pop());
		assertEquals(7,prueba.pop());
	}
	
	public void testPush()
	{
		prueba.push(100);
		assertEquals(100,prueba.readTop());
		
		setUp1();
		assertEquals(9, prueba.readTop());
	}
	
	public void testReadTop()
	{
		setUp1();
		assertEquals(9,prueba.readTop());
	}
	
	public void testIsEmpty()
	{
		assertEquals(true,prueba.isEmpty());
		setUp1();
		assertEquals(false, prueba.isEmpty());
	}
	
	public void testGetSize()
	{
		assertEquals(0,prueba.getSize());
		setUp1();
		assertEquals(10, prueba.getSize());
	}
}


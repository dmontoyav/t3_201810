package api;

import model.vo.Taxi;
import model.vo.Service;

/**
 * Basic API for testing the functionality of the TaxiTrip manager
 */
public interface ITaxiTripsManager {

	/**
	 * Method to load the services of a specific taxi 
	 * The services are loaded in both a Stack and a Queue 
	 * @param servicesFile - path to the JSON file with taxi services 
	 * @param taxiId - taxiId of interest 
	 */
	public void loadServices(String serviceFile);
	

    /**
	 * Method to return the number of services in inverse order by trip_start_timestamp.
	 * The search applies to the services in the Stack
	 * @return array with two integer positions: number of services in inverse order at position 0, and number of services not in inverse order at position 1
	 */
	//public int [] servicesInInverseOrder();
	
    /**
	 * Method to return the number of services in order by trip_start_timestamp.
	 * The search applies to the services in the Queue
	 * @return array with two integer positions: number of services in order at position 0, and number of services not in order at position 1
	 */
	//public int [] servicesInOrder();


	
}

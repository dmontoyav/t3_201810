package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	public String trip_id;
	public String taxi_id;
	public int trip_seconds;
	public double trip_miles;
	public double trip_total;
	public int dropoff_community_area;
	public String trip_start_timestamp;
	public String company;
	
	private RangoFechaHora trip_timestamp;
	
	public Service(String pTripID, String pTaxiID, int pTripSeconds, double pTripMiles, double pTripTotal, int pCommunityArea, RangoFechaHora ptrip_timestamp, String pCompany)
	{
		trip_id=pTripID;
		taxi_id=pTaxiID;
		trip_seconds=pTripSeconds;
		trip_miles =pTripMiles;
		trip_total =pTripTotal;
		dropoff_community_area=pCommunityArea;
		trip_timestamp = ptrip_timestamp;
		company = pCompany;
	}
	
	public String getFechaInicial()
	{
		return trip_timestamp.getFechaInicial();
	}
	public String getFechaFinal()
	{
		return trip_timestamp.getFechaFinal();
	}
	public String getHoraInicial()
	{
		return trip_timestamp.getHoraFinal();
	}
	public String getHoraFinal()
	{
		return trip_timestamp.getHoraFinal();
	}
	
	public String getTripStartTimeStamp()
	{
		return trip_start_timestamp;
	}
	public int getCommunityArea()
	{
		return dropoff_community_area;
	}
	
	public String getCompany()
	{
		return company;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return trip_total;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		if(o.trip_id.equals(trip_id))
			return 1;
		else
		{
			return 0;
		}
		
	}
}

package model.vo;

import java.util.Comparator;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	public String taxi_id;
	public String company; 
	public Taxi hijoDerecha;
	public Taxi hijoIzquierda;
	public Taxi padre;
	public int indice;
	public double distanciaEnPeriodo;
	public double plataEnPeriodo;
	public int serviciosEnPeriodo;

	public Taxi(String pTaxiId , String pCompany)
	{
		taxi_id = pTaxiId;
		company = pCompany;
		distanciaEnPeriodo=0;
		serviciosEnPeriodo=0;
		plataEnPeriodo=0;
	}

	public double getPlataEnPeriodo()
	{
		return plataEnPeriodo;
	}

	public void addPlataEnPeriodo(double nuevo)
	{
		plataEnPeriodo += nuevo;
	}

	public double getDistanciaEnPeriodo()
	{
		return distanciaEnPeriodo;
	}

	public void addDistanciaEnPeriodo(double nuevo)
	{
		distanciaEnPeriodo += nuevo;   
	}

	public int getServiciosEnPeriodo()
	{
		return serviciosEnPeriodo;
	}

	public void addServiciosEnPeriodo(int nuevo)
	{
		serviciosEnPeriodo += nuevo;
	}
	public int getIndice()
	{
		return indice;
	}

	public void setIndice(int nuevo)
	{
		indice = nuevo;
	}

	public Taxi getHijoDerecha()
	{
		return hijoDerecha;
	}

	public void setHijoDerecha(Taxi nuevo)
	{
		hijoDerecha = nuevo;
	}

	public Taxi getHijoIzquierda()
	{
		return hijoIzquierda;
	}

	public void setHijoIzquierda(Taxi nuevo)
	{
		hijoIzquierda = nuevo;
	}

	public Taxi getPadre()
	{
		return padre;
	}

	public void setPadre(Taxi nuevo)
	{
		padre = nuevo;
	}
	public Taxi( String pTaxiId)
	{
		taxi_id = pTaxiId;
		company = null;
	}

	/**
	 * @return id - taxi_id
	 */

	public String getTaxiId() {
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		return company;
	}

	public static class ComparadorPorId implements Comparator<Taxi>
	{

		@Override
		public int compare(Taxi o1, Taxi o2) {
			return (o1.getTaxiId().compareTo(o2.getTaxiId()));	
		}
		
	}
	@Override
	public int compareTo(Taxi o) {
		int i ;
		if(getTaxiId().equals(o.getTaxiId()))
		{
			i=1;
		}
		else
		{
			i=0;
		}
		return i;
	}

}

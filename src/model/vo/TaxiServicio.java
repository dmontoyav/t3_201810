package model.vo;

public class TaxiServicio {

	public String trip_id;
	public String taxi_id;
	public int trip_seconds;
	public double trip_miles;
	public double trip_total;
	public int dropoff_community_area;
	public String company;
	public String trip_start_timestamp;
	public String trip_end_timestamp;
	public RangoFechaHora TripTimeStamp;
	
	public TaxiServicio(String pTaxiID, String pCompany,String pTripID, int pTripSeconds, double pTripMiles, double pTripTotal, int pCommunityArea, String pTripStartTimeStamp, String pTripEndTimeStamp )
	{
		company=pCompany;
		trip_id=pTripID;
		taxi_id=pTaxiID;
		trip_seconds=pTripSeconds;
		trip_miles =pTripMiles;
		trip_total =pTripTotal;
		dropoff_community_area=pCommunityArea;
		trip_start_timestamp = pTripStartTimeStamp;
		trip_end_timestamp = pTripEndTimeStamp;
		
	}
	
	public String getTripStartTimeStamp()
	{
		return trip_start_timestamp;
	}
	
	public String getTripEndTimeStamp()
	{
		return trip_end_timestamp;
	}
	public Taxi createTaxi()
	{
		return new Taxi(taxi_id,company);
	}
	public RangoFechaHora createFechaHoraInicio()
	{
		String[] inicio = asignarFechayHoraInicio();
		String[] fin = asignarFechayHoraFin();
		TripTimeStamp = new RangoFechaHora(inicio[0], fin[0], inicio[1], fin[1]);
		return TripTimeStamp;
	}
	public String[] asignarFechayHoraInicio()
	{
		String [] fechaHoraInicio;
		fechaHoraInicio = trip_start_timestamp.split("T");
		return fechaHoraInicio;
	}
	public String[] asignarFechayHoraFin()
	{
		String [] fechaHoraFin;
		fechaHoraFin = trip_end_timestamp.split("T");
		return fechaHoraFin;
	}
	public Service createService()
	{
		createFechaHoraInicio();
		Service servicio = new Service(trip_id,taxi_id,trip_seconds,trip_miles,trip_total,dropoff_community_area,TripTimeStamp,company);
		return servicio;
	}
	public String getCompany()
	{
		return company;
	}
	public String getTripId()
	{
		return trip_id;
	}
	public String getTaxiId(){
		return taxi_id;
	}
	public int getTripSeconds()
	{
		return trip_seconds;
	}
	public double getTripMiles()
	{
		return trip_miles;
	}
	public double getTripTotal()
	{
		return trip_total;
	}
	public int getDropoffCommunityArea()
	{
		return dropoff_community_area;
	}
}

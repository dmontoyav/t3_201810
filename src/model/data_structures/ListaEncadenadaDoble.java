package model.data_structures;

import java.util.Iterator;

public class ListaEncadenadaDoble <T extends Comparable<T>> implements Iterable, LinkedList<T>{

	private Node<T> first;
	public Node<T> current;
	private Node<T> last;
	private int size;
	
	@Override
	//Agregar al final
	public void addLast(T elem) {
		if (elem != null)
		{
			Node<T> nuevo = new Node(elem);
			if(first == null)
			{
				first = nuevo;
				last = nuevo;
				current = nuevo;
				size++;
			}
			else
			{
				last.cambiarSiguiente(nuevo);
				nuevo.cambiarAnterior(last);;
				nuevo.cambiarSiguiente(null);
				last=nuevo;
				size++;
			}
		}
	}

	public void addFirst(T elem)
	{
		Node<T> nuevo = new Node(elem);
		if(elem != null)
		{
			if(first == null)
			{
				first = nuevo;
				last = nuevo;
				current = nuevo;
				size++;
			}
			else
			{
				nuevo.cambiarSiguiente(first);
				first.cambiarAnterior(nuevo);
				nuevo.cambiarAnterior(null);
				first = nuevo;
				size++;
			}
		}
	}
	@Override
	public void delete(T k) 
	{
		if (first.getItem().equals(k))
		{
			Node temp = first.getNext();
			first = temp; 
		}
		else if (last.getItem().equals(k)) 
		{
			Node temp = last.getPrevious();
			last = temp;
		}
		else
		{
			current = first.getNext();
			int i = 1;
			while(!current.getItem().equals(k) && i < size)
			{
				current = current.getNext();
				i++;
			}
			Node anterior = current.getPrevious();
			Node siguiente = current.getNext();
			anterior.cambiarSiguiente(siguiente);
			siguiente.cambiarAnterior(anterior);
			current = null;
		}
		
		
		size--;
	}

	public T deleteFirst()
	{
		T temp2 = first.getItem();
		if(first!=null)
		{
			Node temp = first.getNext();
			first = temp;
			size--;
		}
		return temp2;
	}
	
	public T deleteLast()
	{
		T temp2 = last.getItem();
		if (last != null)
		{
			Node temp = last.getPrevious();
			last=temp;
			size--;
		}
		return temp2;
	}
	@Override
	public T get(T elem) {
		current = first;
		int i = 0;
		while (!current.getItem().equals(elem) && i < size)
		{
			current = current.getNext();
			i++;
		}
		return (T)current.getItem();
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public T get(int posicion) {
		if (posicion < 0 || posicion > size)
		{
			System.out.println("Error");
			return null;
		}
		else if(posicion == 1)
		{
			return first.getItem();
		}
		else if(posicion == size - 1)
		{
			return last.getItem();
		}
		else
		{
			current = first;
			int i = 0;
			while (i < posicion)
			{
				current = current.getNext();
				i++;
			}
			return current.getItem();
		}
	}

	@Override
	public void listing() 
	{
		current = first;	
	}

	@Override
	public T getCurrent() {
		return (T) current.getItem();
	}

	@Override
	public T next() {
		T devolver = (T) current.getNext().getItem();
		return devolver;
	}

	@Override
	public Iterator<T> iterator() {
		return (Iterator<T>) current.getItem();
	}
	
	public void swap(Node<T> nuevo)
	{
		Node<T> siguiente = current.getNext();
		Node<T> anterior = nuevo.getPrevious();
		current.cambiarSiguiente(nuevo.getNext());
		nuevo.cambiarSiguiente(siguiente);
		nuevo.cambiarAnterior(current.getPrevious());
		current.cambiarAnterior(anterior);
	}
	
	public T getLast()
	{
		return last.getItem();
	}
	public T getFirst()
	{
		return first.getItem();
	}

}

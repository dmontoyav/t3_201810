package model.data_structures;

public class Queue <T extends Comparable<T>> implements IQueue<T>{

	private ListaEncadenadaDoble<T> queue;
	
	public Queue()
	{
		queue = new ListaEncadenadaDoble<T>();
		
	}
	@Override
	public void enqueue(T item) {
		// TODO Auto-generated method stub
		queue.addLast(item);
		
	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		T aux = queue.getFirst();
		queue.delete(queue.getFirst());
		return aux;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(queue.size() == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	public int size()
	{
		return queue.size();
	}
	
	
}

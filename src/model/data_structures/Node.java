package model.data_structures;

public class Node <T extends Comparable<T>>{
	
	public T item;
	public Node<T> next;
	public Node<T> previous;
	
	public Node()
	{
		next = null;
		previous = null;
	}
	
	public Node(T data)
	{
		this.item=data;
	}
	public Node(T data, Node<T> siguiente, Node<T> anterior)
	{
		this.item=data;
		this.next = siguiente;
		this.previous = anterior;
	}
	
	public T getItem()
	{
		return item;
	}
	public Node<T> getNext()
	{
		return next;
	}
	public Node<T> getPrevious()
	{
		return previous;
	}
	public void cambiarAnterior(Node<T> nAnterior)
	{
		previous = nAnterior;
	}
	public void cambiarSiguiente(Node<T> nSiguiente)
	{
		next = nSiguiente;
	}
}


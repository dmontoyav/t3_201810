package model.data_structures;

import java.util.Iterator;

public class Stack <T extends Comparable<T>> implements Iterable, IStack<T>
{

	private ListaEncadenadaDoble<T> lista;
	public Stack ()
	{
		lista =new ListaEncadenadaDoble<T>(); 
	}
	@Override
	public void push(T item) {
		lista.addLast(item);
	}

	@Override
	public T pop() {
		T pop = lista.deleteLast();
		return pop;
		
	}

	public T readTop()
	{
		T top = lista.getLast();
		return top;
	}
	@Override
	public boolean isEmpty() {
		boolean vacio = false;
		if (lista.size()==0)
		{
			vacio = true;
		}
		return vacio;
	}
	
	public int getSize()
	{
		return lista.size();
	}

	@Override
	public Iterator iterator() {
		Iterator iter = lista.iterator();
		return iter;
	}

}

package model.data_structures;

import model.vo.Taxi;

public class HeapSort {

	private void Sort(Taxi[] pTaxis)
	{
		int n = pTaxis.length;
		for (int k = n/2; k > 0; k--)
		{
			volverHeap(pTaxis, k,n);
		}
		for(int i = n;i>0;i--)
		{
			swap(pTaxis,0,i);
			n--;
			volverHeap(pTaxis,0,n);
		}
	}
	private void volverHeap(Taxi[] pTaxis, int d,int total)
	{
		int posIzq = 2*d;
		int posDer = 2*d+1;
		int posPadre = d;
		Taxi HijoIzquierdo = pTaxis[posIzq];
		Taxi HijoDerecho = pTaxis[posDer];
		Taxi Padre = pTaxis[posPadre];
		if(posIzq <= total && HijoIzquierdo.getTaxiId().compareTo(Padre.getTaxiId())>0)
		{
			posPadre = posIzq; 
		}
		if(posDer <= total && HijoDerecho.getTaxiId().compareTo(Padre.getTaxiId())>0)
		{
			posPadre = posDer;
		}
		if(posPadre != d)
		{
			swap(pTaxis,d,posPadre);
			volverHeap(pTaxis, posPadre, total);
		}
	}
	private void swap(Taxi[] pTaxis, int d, int posPadre)
	{
		Taxi temp = pTaxis[d];
		pTaxis[d]=pTaxis[posPadre];
		pTaxis[posPadre]=temp;
	}
}

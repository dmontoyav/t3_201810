package model.logic;

import java.io.FileReader;
import java.util.ArrayList;

import com.google.gson.Gson;

import api.ITaxiTripsManager;
import model.data_structures.Heap;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.ListaEncadenadaDoble;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Taxi;
import model.vo.TaxiServicio;
import model.vo.RangoFechaHora;
import model.vo.Service;


public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	Stack<Service> stackServicios = new Stack();
	Queue<Service> queueServicios = new Queue();
	ArrayList<Service> misServicios = new ArrayList<Service>();
	ArrayList<Taxi> taxisEnrango = new ArrayList<Taxi>();

	public void loadServices(String serviceFile) {
		// TODO Auto-generated method stub

		Gson gson = new Gson();
		try
		{

			TaxiServicio myType[] = gson.fromJson(new FileReader(serviceFile),TaxiServicio[].class);

			for(int i =0;i<myType.length;i++)
			{
				Service servicio = myType[i].createService();
				misServicios.add(servicio);		
			}

			System.out.println(misServicios.size());
		}
		catch(Exception e)
		{
			System.out.println("*Smoke bomb*");
		}

	}


	public boolean estaEnRango(Service s, RangoFechaHora r)
	{
		if(s.getFechaFinal().compareTo(r.getFechaFinal())<=0&&s.getFechaInicial().compareTo(r.getFechaInicial())>=0&&s.getHoraFinal().compareTo(r.getHoraFinal())<=0&&s.getHoraInicial().compareTo(r.getHoraInicio())>=0)
		{
			return true;
		}
		return false;
	}

	public void generarHeap(RangoFechaHora r)
	{
		Taxi.ComparadorPorId comparador = new Taxi.ComparadorPorId();
		ArrayList<Taxi> respuesta = new ArrayList<Taxi>();
		Heap<Taxi> respuestaHeap = new Heap<Taxi>(100,comparador,Heap.MAX_HEAP);
		for(int i=0; i<misServicios.size();i++)
		{
			boolean existe = false;
			Service actual = misServicios.get(i);
			if(estaEnRango(actual,r))
			{
				for(int k =0; k<respuesta.size();k++)
				{
					Taxi momento = respuesta.get(k);
					if(momento.getTaxiId().equals(actual.getTaxiId()))
					{

						existe = true;
						momento.addServiciosEnPeriodo(1);
						momento.addDistanciaEnPeriodo(actual.getTripMiles());
						momento.addPlataEnPeriodo(actual.getTripTotal());
					}
				}

				if(!existe)
				{
					Taxi newTaxi = new Taxi(actual.getTaxiId(),actual.getCompany());
					newTaxi.addServiciosEnPeriodo(1);
					newTaxi.addDistanciaEnPeriodo(actual.getTripMiles());
					newTaxi.addPlataEnPeriodo(actual.getTripTotal());

					respuesta.add(newTaxi);
				}
			}	
		}

		for(int l=0;l<respuesta.size();l++)
		{
			respuestaHeap.add(respuesta.get(l));
		}
		Taxi[] temp = respuestaHeap.getPQ();
		
		for(int m=0; temp.length>m;m++)
		{
			System.out.println("Taxi ID: " + temp[m].getTaxiId());
			System.out.println("Distancia en Rango: " + temp[m].getDistanciaEnPeriodo());
			System.out.println("Ganancias en Rango: " + temp[m].getPlataEnPeriodo());
			System.out.println("Servicios en Rango: " + temp[m].getServiciosEnPeriodo());
			
			taxisEnrango.add(temp[m]);
		}

	}

	/**
	 * Desde aqui empieza el heapsort
	 * @param pTaxis
	 */

	public String arregloOrdenado()
	{
		heapSort(taxisEnrango);
		String respuesta = new String();
		for (int i = 0; i<taxisEnrango.size();i++)
		{
			Taxi actual = taxisEnrango.get(i);
			System.out.println("Taxi Id: " + actual.getTaxiId() + " Servicios en el periodo: " + actual.getServiciosEnPeriodo() + "; ");
			respuesta+= actual.getTaxiId() + " " + actual.getServiciosEnPeriodo() + "; ";
		}
		return respuesta;
	}
	private void heapSort(ArrayList<Taxi> pTaxis)
	{
		int n = pTaxis.size();
		for (int k = n/2; k > 0; k--)
		{
			volverHeap(pTaxis, k,n);
		}
		for(int i = n;i>0;i--)
		{
			swap(pTaxis,0,i);
			n--;
			volverHeap(pTaxis,0,n);
		}
	}
	private void volverHeap(ArrayList<Taxi> pTaxis, int d,int total)
	{
		int posIzq = 2*d+2;
		int posDer = 2*d+1;
		int posPadre = d;
		Taxi HijoIzquierdo = pTaxis.get(posIzq);
		Taxi HijoDerecho = pTaxis.get(posDer);
		Taxi Padre = pTaxis.get(posPadre);
		if(posIzq <= total && HijoIzquierdo.getTaxiId().compareTo(Padre.getTaxiId())>0)
		{
			posPadre = posIzq; 
		}
		if(posDer <= total && HijoDerecho.getTaxiId().compareTo(Padre.getTaxiId())>0)
		{
			posPadre = posDer;
		}
		if(posPadre != d)
		{
			swap(pTaxis,d,posPadre);
			volverHeap(pTaxis, posPadre, total);
		}
	}
	private void swap(ArrayList<Taxi> pTaxis, int d, int posPadre)
	{
		Taxi tempd = pTaxis.get(d);
		Taxi tempP = pTaxis.get(posPadre);
		pTaxis.set(d, tempP);
		pTaxis.set(posPadre, tempd);
	}
	
	
	

}
